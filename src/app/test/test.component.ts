import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css'],
  styles:[`
    .text-success{
      color :green;
    }`
  ]
})
export class TestComponent implements OnInit {

  public name = 'Riddhi Rathod';
  public greeting = "";
  public displayName = false;
  public color = 'orange';
  public colors = ['red','green','blue','orange'];

  constructor() { }

  ngOnInit(): void {
  }

  greetUser(){
    return `Hello `+this.name;
  }

  onClick(){
    console.log("hello");
    //this.greeting = event.type;
  }

}
