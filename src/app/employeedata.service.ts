import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeedataService {
  
  constructor() { }

  getEmployees(){
    return [
      {'id':12, 'name':'Abc', 'age':30},
      {'id':13, 'name' : 'Raj', 'age': 25},
      {'id':14, 'name':'Test', 'age':20}
    ];
  }
}
