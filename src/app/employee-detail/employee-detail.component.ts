import { Component, OnInit } from '@angular/core';
import { EmployeedataService } from '../employeedata.service';
@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {

  public employees = [] as any;
  constructor(private _employeedataService : EmployeedataService) { }

  ngOnInit(): void {
    this.employees = this._employeedataService.getEmployees();
  }

}
