import { Component, OnInit } from '@angular/core';
import { EmployeedataService } from '../employeedata.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  public employees = [] as any;

  constructor(private _employeedataService: EmployeedataService) { }

  ngOnInit(): void {
    this.employees = this._employeedataService.getEmployees();
  }

}
